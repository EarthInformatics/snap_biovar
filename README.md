# snap_biovar

## Introduction

This is the repository of `snap_biovar` it is a python implementation of the biophysical variable 
(LAI, Fcover, FAPAR) retrieval toolbox integrated in the [ESA SNAP](https://earth.esa.int/eogateway/tools/snap) 
processing software for Sentinel2 satellite observations.

However, running the biophysical variable retrieval is inconvenient when having the process many scenes entirely
when you are only interested in average values for certain agricultural fields. Therefore, we prefer to run
the biovar retrieval separately on the S2 reflectance values at the field level. This toolbox does exactly that.
It takes a dataframe with timeseries of reflectance values of the different channels and computes the biovar values.

Since the biovar retrieval is basically a two-layer neural network, converting it to python is straightforward.

## Getting started

The snap_biovar toolbox can be installed with:

`pip install https://git.wur.nl/EarthInformatics/snap_biovar/-/archive/main/snap_biovar-main.zip`

The dependencies are: 
- pandas
- astral

The snap_biovar toolbox is most useful in combination with grompy: https://git.wur.nl/wit015/grompy
but it is not a strict requirement. 

## Usage

For an example, see the notebook in the `notebooks` folder.

See also the documentation of `snap_biovar.compute_biovar`

## Support

For questions ask me (allard.dewit@wur.nl)

## Authors and acknowledgment

Allard de Wit, June 2023

Javascript code is copied from:
https://github.com/sentinel-hub/custom-scripts/tree/master/sentinel-2/fapar
https://github.com/sentinel-hub/custom-scripts/tree/master/sentinel-2/lai
https://github.com/sentinel-hub/custom-scripts/tree/master/sentinel-2/fcover

Then converted to python.


## License
Original javascript code is licensed under Creative Commons Attribution-ShareAlike 4.0 International Public License.
So is this code, see LICENSE.md




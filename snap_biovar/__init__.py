"""Python implementation of the SNAP Biophysical Variable retrieval toolkit for Sentinel2

The code computes the LAI, FAPAR and FCOVER values from a dataframe with a timeseries
of Sentinel2 observations.

https://github.com/sentinel-hub/custom-scripts/tree/master/sentinel-2/fapar
https://github.com/sentinel-hub/custom-scripts/tree/master/sentinel-2/lai
https://github.com/sentinel-hub/custom-scripts/tree/master/sentinel-2/fcover

"""
from astral.location import Location, LocationInfo
import pandas as pd

from .fapar import compute_fapar
from .lai import compute_lai
from .fcover import compute_fcover

__version__ = "1.0.2"

def compute_biovar(df_s2, longitude, latitude, timezone="Europe/Amsterdam"):
    """

    :param df_s2: The dataframe with Sentinel2 timeseries data
    :param longitude: The site longitude
    :param latitude: The site latitude
    :param timezone: The timezone
    :param sensor_zenith: the view zenith angle
    :param sensor_azimuth: the view azimuth angle
    :return: the same dataframe with several columns added:
        - lai: the calculated leaf area index
        - fapar: the calculated fraction of absorbed PAR
        - fcover: the calculateion fractional green canopy coverage
        - day_tz: the timezone localized date/time
        - solar_azimuth: local solar azimuth for this date/time
        - solar_zenith: local solar zenith for this date/time

    The input dataframe with Sentinel2 data is taken from grompy 1.3 but basically looks like::

                           day    B02    B03    B04     B05     B06     B07     B08     B11     B12     B8A    NDVI sensor  sensor_zenith  sensor_azimuth
        day
        2021-07-06  2021-07-06  476.0  825.0  513.0  1451.0  3950.0  4868.0  5096.0  1664.0   777.0  5214.0  0.8192    S2B            3.3           128.0
        2021-08-23  2021-08-23  236.0  569.0  195.0   982.0  3678.0  4521.0  4609.0  1670.0   729.0  4762.0  0.9202    S2A            8.9           288.6
        2021-09-09  2021-09-09  343.0  694.0  302.0  1131.0  3721.0  4431.0  4430.0  2019.0   943.0  4909.0  0.8728    S2A            3.3           128.0
        2021-09-14  2021-09-14  307.0  657.0  270.0  1083.0  3226.0  3819.0  3965.0  1948.0   941.0  4144.0  0.8751    S2B            3.3           128.0
        2021-10-04  2021-10-04  317.0  649.0  370.0  1243.0  3014.0  3544.0  3668.0  1908.0   949.0  3928.0  0.8204    S2B            3.3           128.0
        2021-10-09  2021-10-09  353.0  718.0  431.0  1214.0  2981.0  3436.0  3661.0  1948.0   967.0  3832.0  0.7907    S2A            3.3           128.0
        2021-10-24  2021-10-24  293.0  669.0  396.0  1197.0  3006.0  3452.0  3690.0  1779.0   905.0  3735.0  0.8095    S2B            3.3           128.0
        2021-10-29  2021-10-29  260.0  585.0  376.0  1010.0  2327.0  2649.0  2700.0  1438.0   729.0  2913.0  0.7532    S2A            3.3           128.0
    """


    loc = LocationInfo("", "", timezone, latitude, longitude)
    S2_bands = ["B02", "B03", "B04", "B05", "B06", "B07", "B08", "B11", "B12", "B8A", "NDVI"]
    # Scale to reflectance values
    df_s2.loc[:,S2_bands] /= 10000
    # We assume a local overpass time of 10:30 in the morning (adding 10.5 hours)
    tmp = pd.to_datetime(df_s2.day) + pd.Timedelta(10.5, unit="h")
    df_s2["day_tz"] = tmp.dt.tz_localize(timezone)
    df_s2["solar_azimuth"] = df_s2.day_tz.apply(Location(loc).solar_azimuth)
    df_s2["solar_zenith"] = df_s2.day_tz.apply(Location(loc).solar_zenith)
    df_s2["fapar"] = compute_fapar(df_s2)
    df_s2["lai"] = compute_lai(df_s2)
    df_s2["fcover"] = compute_fcover(df_s2)

    return df_s2
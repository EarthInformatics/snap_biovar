import math
import numpy as np
degToRad = math.pi / 180

from .generics import tansig, normalize, denormalize


def compute_fcover(input):
    b03_norm = normalize(input.B03, 0, 0.253061520471542)
    b04_norm = normalize(input.B04, 0, 0.290393577911328)
    b05_norm = normalize(input.B05, 0, 0.305398915248555)
    b06_norm = normalize(input.B06, 0.006637972542253, 0.608900395797889)
    b07_norm = normalize(input.B07, 0.013972727018939, 0.753827384322927)
    b8a_norm = normalize(input.B8A, 0.026690138082061, 0.782011770669178)
    b11_norm = normalize(input.B11, 0.016388074192258, 0.493761397883092)
    b12_norm = normalize(input.B12, 0, 0.493025984460231)
    viewZen_norm = normalize(np.cos(input.sensor_zenith * degToRad), 0.918595400582046, 1)
    sunZen_norm  = normalize(np.cos(input.solar_zenith * degToRad), 0.342022871159208, 0.936206429175402)
    relAzim_norm = np.cos((input.solar_azimuth - input.sensor_azimuth) * degToRad)

    inp = (b03_norm,b04_norm,b05_norm,b06_norm,b07_norm,b8a_norm,
           b11_norm,b12_norm, viewZen_norm,sunZen_norm,relAzim_norm)
    n1 = neuron1(*inp)
    n2 = neuron2(*inp)
    n3 = neuron3(*inp)
    n4 = neuron4(*inp)
    n5 = neuron5(*inp)

    l2 = layer2(n1, n2, n3, n4, n5)

    fcover = denormalize(l2, 0.000181230723879, 0.999638214715)
    return fcover


def neuron1(b03_norm,b04_norm,b05_norm,b06_norm,b07_norm,b8a_norm,
            b11_norm,b12_norm, viewZen_norm,sunZen_norm,relAzim_norm):
    value = (
    - 1.45261652206
    - 0.156854264841  * b03_norm
    + 0.124234528462  * b04_norm
    + 0.235625516229  * b05_norm
    - 1.8323910258    * b06_norm
    - 0.217188969888  * b07_norm
    + 5.06933958064   * b8a_norm
    - 0.887578008155  * b11_norm
    - 1.0808468167    * b12_norm
    - 0.0323167041864 * viewZen_norm
    - 0.224476137359  * sunZen_norm
    - 0.195523962947  * relAzim_norm
    )

    return tansig(value)


def neuron2(b03_norm,b04_norm,b05_norm,b06_norm,b07_norm,b8a_norm,
            b11_norm,b12_norm, viewZen_norm,sunZen_norm,relAzim_norm):
    value = (
        - 1.70417477557
        - 0.220824927842  * b03_norm
        + 1.28595395487   * b04_norm
        + 0.703139486363  * b05_norm
        - 1.34481216665   * b06_norm
        - 1.96881267559   * b07_norm
        - 1.45444681639   * b8a_norm
        + 1.02737560043   * b11_norm
        - 0.12494641532   * b12_norm
        + 0.0802762437265 * viewZen_norm
        - 0.198705918577  * sunZen_norm
        + 0.108527100527  * relAzim_norm
    )

    return tansig(value)


def neuron3(b03_norm,b04_norm,b05_norm,b06_norm,b07_norm,b8a_norm,
            b11_norm,b12_norm, viewZen_norm,sunZen_norm,relAzim_norm):
    value = (
        + 1.02168965849
        - 0.409688743281   * b03_norm
        + 1.08858884766    * b04_norm
        + 0.36284522554    * b05_norm
        + 0.0369390509705  * b06_norm
        - 0.348012590003   * b07_norm
        - 2.0035261881     * b8a_norm
        + 0.0410357601757  * b11_norm
        + 1.22373853174    * b12_norm
        + -0.0124082778287 * viewZen_norm
        - 0.282223364524   * sunZen_norm
        + 0.0994993117557  * relAzim_norm
    )

    return tansig(value)


def neuron4(b03_norm,b04_norm,b05_norm,b06_norm,b07_norm,b8a_norm,
            b11_norm,b12_norm, viewZen_norm,sunZen_norm,relAzim_norm):
    value = (
        - 0.498002810205
        - 0.188970957866   * b03_norm
        - 0.0358621840833  * b04_norm
        + 0.00551248528107 * b05_norm
        + 1.35391570802    * b06_norm
        - 0.739689896116   * b07_norm
        - 2.21719530107    * b8a_norm
        + 0.313216124198   * b11_norm
        + 1.5020168915     * b12_norm
        + 1.21530490195    * viewZen_norm
        - 0.421938358618   * sunZen_norm
        + 1.48852484547    * relAzim_norm
    )

    return tansig(value)


def neuron5(b03_norm,b04_norm,b05_norm,b06_norm,b07_norm,b8a_norm,
            b11_norm,b12_norm, viewZen_norm,sunZen_norm,relAzim_norm):
    value = (
        - 3.88922154789
        + 2.49293993709  * b03_norm
        - 4.40511331388  * b04_norm
        - 1.91062012624  * b05_norm
        - 0.703174115575 * b06_norm
        - 0.215104721138 * b07_norm
        - 0.972151494818 * b8a_norm
        - 0.930752241278 * b11_norm
        + 1.2143441876   * b12_norm
        - 0.521665460192 * viewZen_norm
        - 0.445755955598 * sunZen_norm
        + 0.344111873777 * relAzim_norm
        )

    return tansig(value)


def layer2(neuron1, neuron2, neuron3, neuron4, neuron5):
    value = (
        - 0.0967998147811
        + 0.23080586765   * neuron1
        - 0.333655484884  * neuron2
        - 0.499418292325  * neuron3
        + 0.0472484396749 * neuron4
        - 0.0798516540739 * neuron5
    )
    return value

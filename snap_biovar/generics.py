"""Some auxiliary functions used by the SNAP biovar routines
"""


import numpy as np


def normalize(unnormalized, vmin, vmax):
    return 2 * (unnormalized - vmin) / (vmax - vmin) - 1


def denormalize(normalized, vmin, vmax):
    return 0.5 * (normalized + 1) * (vmax - vmin) + vmin


def tansig(v):
  return 2 / (1 + np.exp(-2 * v)) - 1
